     module main
     
     implicit none

     real*8:: pi=3.1415926535897932384626

     integer :: Totalprocs,myid,ierr,rc 

     character(len=20) LogDir,outInitialflowDir, LogID, LogName
     logical:: LogDirhave,outInitialflowDirhave

     
     !!!!!!parameters in the inputfile!!!!!!!!
     integer*8:: maxsteps, outputsteps 
     integer:: StreamBlocks, SpanBlocks 

     !!! Currently, this code only surports divide the blocks in two diractions (stream and span directions)
     !!! However, this is enough for massvie parallel simulation using MPI

     integer:: restartstate, isglobaltime,isexplicit,issteady,Numericalframework
     integer:: restartsteps
     real*8::  CFL,timestep, restarttime
     real*8::  u0, v0, w0, Tp0, rho0, Re, Ma   !!!nondimensional values
     real*8::  T_far                           !!! dimensional value.
     integer:: lap                             !!!!cell in the vitual zone
     real*8::  p0

     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     real*8:: gama,cp,cv
     integer:: FV_Method=1, FD_Method=2
     integer:: FromVeryBeginning=0, FromLaststep=1
     
     
     integer:: Totalblocks
     integer:: imax,jmax,kmax   !!!mesh grid number
     integer:: ib,ie,jb,je,kb,ke  !!! true mesh grid index

     integer:: iq,jq,kq  !!! number of all mesh points  per block


     real*8,pointer,dimension(:,:,:):: x,y,z      !!!grid points coordinates

     character(len=20) BlockName,Gridname

     integer:: Nvar=5
     !!!integer:: Nsp=0   !!!! for further consideration to take multispecises flow into account
     integer*8::istep

     !!!!for boundary condition setup
     integer:: periodic=0,  inlet=1, outlet=2, farfield=3,viscouswall_Adiabatic=4,viscouswall_ConstTemperature=5, &
               viscouswall_ConstHeatFlux=6, inviscidwall=7, ConstState=8, SpecificBC=9
     integer:: IminBoundtype, ImaxBoundtype, JminBoundtype, JmaxBoundtype, KminBoundtype,KmaxBoundtype


     !!! transfer length for coordinates (should be set in inputfile) if periodic boundary conditions are used     
     real*8:: DLx_Itransfer, DLy_Itransfer, DLz_Itransfer
     real*8:: DLx_Jtransfer, DLy_Jtransfer, DLz_Jtransfer 
     real*8:: DLx_Ktransfer, DLy_Ktransfer, DLz_Ktransfer   


     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !!!  Variables for Finite Volume Method
     real*8,pointer,dimension(:,:,:):: xc,yc,zc,vol   
     !!! cell center coordinates and cell volume, just the cells in the computational zone are needed
     !!! xc(1:iq-1,1:jq-1,1:kq-1),yc(1:iq-1,1:jq-1,1:kq-1),zc(1:iq-1,1:jq-1,1:kq-1)
     !!! In I direction we just use xc(1:iq-1,jb:je-1,kb:ke-1),yc(1:iq-1,jb:je-1,kb:ke-1),zc(1:iq-1,jb:je-1,kb:ke-1)
     !!! In J direction we just use xc(ib:ie-1,1:jq-1,kb:ke-1),yc(ib:ie-1,1:jq-1,kb:ke-1),zc(ib:ie-1,1:jq-1,kb:ke-1)
     !!! In K direction we just use xc(ib:ie-1,jb:je-1,1:kq-1),zc(ib:ie-1,jb:je-1,1:kq-1),zc(ib:ie-1,jb:je-1,1:kq-1)
     !!! vol(ib:ie-1,jb:je-1,kb:ke-1)
     real*8,pointer,dimension(:,:,:):: allim,alrim,alljm,alrjm,allkm,alrkm !!! using for interplotating variables at cell center to fece center
 

     real*8:: dx,dy,dz !!!!!using for uniform grid
     
     real*8,pointer,dimension(:,:,:):: Si,Sj,Sk,Six,Siy,Siz,Sjx,Sjy,Sjz,Skx,Sky,Skz       !!!!



     real*8,pointer,dimension(:,:,:,:)::Qp, Qc, Qc_n1, Qc_n2 !,mu_c
     !!! Qp(i,j,k,0)=Tp(i,j,k)  !!!here Tp is static temperature at each cell
     !!! Qp(i,j,k,1)=rho(i,j,k)   
     !!! Qp(i,j,k,2)=u(i,j,k)
     !!! Qp(i,j,k,3)=v(i,j,k)
     !!! Qp(i,j,k,4)=w(i,j,k)
     !!! Qp(i,j,k,5)=p(i,j,k)
     !!! if there are multispcises to take into account, we should have the following addiotional variables:
     !!! Qp(i,j,k,isp+5)=rho(i,j,k)*massf(i,j,k,isp)
     !!! Here, we should have 1<=i<=iq-1, 1<=j<=jq-1, 1<=k<kq-1 for variables at cell center, including the cells in the virtual zone

     !!! Qc(i,j,k,1)=rho(i,j,k)   
     !!! Qc(i,j,k,2)=rhou(i,j,k)
     !!! Qc(i,j,k,3)=rhov(i,j,k)
     !!! Qc(i,j,k,4)=rhow(i,j,k)
     !!! Qc(i,j,k,5)=rhoE(i,j,k)
     !!! if there are multispcises to take into account, we should have the following addiotional variables:
     !!! Qc(i,j,k,isp+5)=rho(i,j,k)*massf(i,j,k,isp)
     !!! Here, we should have 1<=i<=iq-1, 1<=j<=jq-1, 1<=k<kq-1 for variables at cell center, including the cells in the virtual zone
     !!! Of course, QC_n1 and Qc_n2 follow the above rules as well.

     real*8,pointer,dimension(:,:,:,:)::netFlux    
     !!! netFlux(ib:ie-1,jb:je-1,kb:ke-1,1:Nvar) is the net flux for each cell in the computational zone.
     !!! For I direction:  netFlux(i,j,k,ivar)=Flux(i,j,k,ivar)-Flux(i+1,j,k,ivar)  and should take the direction of normal vector into account!!!

     real*8,pointer,dimension(:,:,:,:)::Flux,QR,QL !, mu_face   !!! mu_f is the coefficient of viscousty
     !!! Flux(ib:ie,jb:je,kb:ke,1:Nvar)  !!! Here Nvar==5, Flux is the fluxes at cell interfaces.
     !!! Addtional, we should note that:
     !!! for I direction Flux(ib:ie,jb:je-1,kb:ke-1,1:Nvar) are valid,
     !!! for J dirction  Flux(ib:ie-1,jb:je,kb:ke-1,1:Nvar) are valid,
     !!! and for K direction Flux(ib:ie-1,jb:je-1,kb:ke,1:Nvar) are valid.
     !!! Of course, the reconstruction values of right states (QR), left states (QL) and mu follow the above rules as well. 



     real*8,pointer,dimension(:,:,:):: IminBC_Qp,ImaxBC_Qp,JminBC_Qp,JmaxBC_Qp,KminBC_Qp,KmaxBC_Qp  
     !!! the primary variables at the boundary faces, using them to recover the boundary condtions after reconstrution anf interplotation
     real*8,pointer,dimension(:,:):: IminBC_mu,ImaxBC_mu,JminBC_mu,JmaxBC_mu,KminBC_mu,KmaxBC_mu

     real*8,pointer,dimension(:,:,:):: IminBC_Qpdx,ImaxBC_Qpdx,JminBC_Qpdx,JmaxBC_Qpdx,KminBC_Qpdx,KmaxBC_Qpdx
     real*8,pointer,dimension(:,:,:):: IminBC_Qpdy,ImaxBC_Qpdy,JminBC_Qpdy,JmaxBC_Qpdy,KminBC_Qpdy,KmaxBC_Qpdy
     real*8,pointer,dimension(:,:,:):: IminBC_Qpdz,ImaxBC_Qpdz,JminBC_Qpdz,JmaxBC_Qpdz,KminBC_Qpdz,KmaxBC_Qpdz

     real*8,pointer,dimension(:,:):: IminBC_ksi_x,IminBC_ksi_y,IminBC_ksi_z
     real*8,pointer,dimension(:,:):: IminBC_eta_x,IminBC_eta_y,IminBC_eta_z
     real*8,pointer,dimension(:,:):: IminBC_zeta_x,IminBC_zeta_y,IminBC_zeta_z

     real*8,pointer,dimension(:,:):: ImaxBC_ksi_x,ImaxBC_ksi_y,ImaxBC_ksi_z
     real*8,pointer,dimension(:,:):: ImaxBC_eta_x,ImaxBC_eta_y,ImaxBC_eta_z
     real*8,pointer,dimension(:,:):: ImaxBC_zeta_x,ImaxBC_zeta_y,ImaxBC_zeta_z

     real*8,pointer,dimension(:,:):: JminBC_ksi_x,JminBC_ksi_y,JminBC_ksi_z
     real*8,pointer,dimension(:,:):: JminBC_eta_x,JminBC_eta_y,JminBC_eta_z
     real*8,pointer,dimension(:,:):: JminBC_zeta_x,JminBC_zeta_y,JminBC_zeta_z

     real*8,pointer,dimension(:,:):: JmaxBC_ksi_x,JmaxBC_ksi_y,JmaxBC_ksi_z
     real*8,pointer,dimension(:,:):: JmaxBC_eta_x,JmaxBC_eta_y,JmaxBC_eta_z
     real*8,pointer,dimension(:,:):: JmaxBC_zeta_x,JmaxBC_zeta_y,JmaxBC_zeta_z

     real*8,pointer,dimension(:,:):: KminBC_ksi_x,KminBC_ksi_y,KminBC_ksi_z
     real*8,pointer,dimension(:,:):: KminBC_eta_x,KminBC_eta_y,KminBC_eta_z
     real*8,pointer,dimension(:,:):: KminBC_zeta_x,KminBC_zeta_y,KminBC_zeta_z

     real*8,pointer,dimension(:,:):: KmaxBC_ksi_x,KmaxBC_ksi_y,KmaxBC_ksi_z
     real*8,pointer,dimension(:,:):: KmaxBC_eta_x,KmaxBC_eta_y,KmaxBC_eta_z
     real*8,pointer,dimension(:,:):: KmaxBC_zeta_x,KmaxBC_zeta_y,KmaxBC_zeta_z

     !real*8,pointer,dimension(:,:):: IminBC1_ksi_x,IminBC1_ksi_y,IminBC1_ksi_z
     !real*8,pointer,dimension(:,:):: IminBC1_eta_x,IminBC1_eta_y,IminBC1_eta_z
     !real*8,pointer,dimension(:,:):: IminBC1_zeta_x,IminBC1_zeta_y,IminBC1_zeta_z
     


     




     










     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !!!  Variables for Finite Difference Method







    
      
   
    end module main
