    subroutine MPISendRec_PointsCoordinates
      use main
      implicit none
      include 'mpif.h'
      integer status(MPI_STATUS_SIZE)

     
      integer:: i,j,k,nlap
      integer:: Inumber,Knumber
      integer :: tag1=1, tag2=2, tag3=3, tag4=4,tag5=5, tag6=6, tag7=7,tag8=8, tag9=9,tag10=10
      !real*8 :: dx1,dy1,dz1
    
     
      !!!!! Here, we need to set the coordinates in stream and span directions
      !!!!! since we just divide the blocks in these two directions
     

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !!!!  the following statement type is not surported by some fortran compiler
      !!!!  when the variables of this type  are used for MPI_SEND/RECV functions
      
      !real*8,pointer,dimension(:,:,:,:)::IminCoor_Send  
      !real*8,pointer,dimension(:,:,:,:)::ImaxCoor_Send


      !real*8,pointer,dimension(:,:,:,:)::IminCoor_Recv  
      !real*8,pointer,dimension(:,:,:,:)::ImaxCoor_Recv


      !real*8,pointer,dimension(:,:,:,:)::KminCoor_Send
      !real*8,pointer,dimension(:,:,:,:)::KmaxCoor_Send

      !real*8,pointer,dimension(:,:,:,:)::KminCoor_Recv
      !real*8,pointer,dimension(:,:,:,:)::KmaxCoor_Recv

      !!!!  the above statement type is not surported by some fortran compiler
      !!!!  when the variables of this type are used for MPI_SENDRECV function
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      !real*8,dimension(:,:,:,:),allocatable::IminCoor_Send  
      !real*8,dimension(:,:,:,:),allocatable::ImaxCoor_Send


      !real*8,dimension(:,:,:,:),allocatable::IminCoor_Recv  
      !real*8,dimension(:,:,:,:),allocatable::ImaxCoor_Recv


      !real*8,dimension(:,:,:,:),allocatable::KminCoor_Send
      !real*8,dimension(:,:,:,:),allocatable::KmaxCoor_Send


      !real*8,dimension(:,:,:,:),allocatable::KminCoor_Recv
      !real*8,dimension(:,:,:,:),allocatable::KmaxCoor_Recv

      real*8,allocatable::IminCoor_Send(:,:,:,:)  
      real*8,allocatable::ImaxCoor_Send(:,:,:,:)


      real*8,allocatable::IminCoor_Recv(:,:,:,:)  
      real*8,allocatable::ImaxCoor_Recv(:,:,:,:)


      real*8,allocatable::KminCoor_Send(:,:,:,:)
      real*8,allocatable::KmaxCoor_Send(:,:,:,:)


      real*8,allocatable::KminCoor_Recv(:,:,:,:)
      real*8,allocatable::KmaxCoor_Recv(:,:,:,:)


      !!!!!!!! Should be noted that the auto index for fortran arry is begin with 1
      allocate(IminCoor_Send(1:lap,jb:je,kb:ke,1:3))
      allocate(ImaxCoor_Send(1:lap,jb:je,kb:ke,1:3))

      allocate(IminCoor_Recv(1:lap,jb:je,kb:ke,1:3))
      allocate(ImaxCoor_Recv(1:lap,jb:je,kb:ke,1:3))



      allocate(KminCoor_Send(ib:ie,jb:je,1:lap,1:3))
      allocate(KmaxCoor_Send(ib:ie,jb:je,1:lap,1:3))

      allocate(KminCoor_Recv(ib:ie,jb:je,1:lap,1:3))
      allocate(KmaxCoor_Recv(ib:ie,jb:je,1:lap,1:3))



     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !!!    sent and receive coordinates for I direction

     do nlap=1,lap
       do j=jb, je
         do k=kb, ke
          IminCoor_Send(nlap,j,k,1)=x(ib+nlap,j,k)
          IminCoor_Send(nlap,j,k,2)=y(ib+nlap,j,k)
          IminCoor_Send(nlap,j,k,3)=z(ib+nlap,j,k)

          ImaxCoor_Send(nlap,j,k,1)=x(ie-lap+nlap-1,j,k)
          ImaxCoor_Send(nlap,j,k,2)=y(ie-lap+nlap-1,j,k)
          ImaxCoor_Send(nlap,j,k,3)=z(ie-lap+nlap-1,j,k)
         enddo
       enddo
     enddo


     Inumber=3*lap*Jmax*Kmax 
     !write(10,*)"Inumber=", Inumber


    
     !!!!send Imaxcoordinates for each blocks and do corresspondding receive
    
     if(myid<SpanBlocks)then
       call MPI_SEND(ImaxCoor_Send,Inumber,MPI_DOUBLE_PRECISION,myid+SpanBlocks,tag1,MPI_COMM_WORLD,ierr)
     elseif(myid>=Totalprocs-SpanBlocks)then
       call MPI_RECV(IminCoor_Recv,Inumber,MPI_DOUBLE_PRECISION,myid-SpanBlocks,tag1,MPI_COMM_WORLD,status,ierr)
     else
       call MPI_SENDRECV(ImaxCoor_Send,Inumber,MPI_DOUBLE_PRECISION,myid+SpanBlocks,tag1, &
                        IminCoor_Recv,Inumber,MPI_DOUBLE_PRECISION,myid-SpanBlocks,tag1,MPI_COMM_WORLD,status,ierr)

     endif

     !!!!send Imincoordinates for each blocks and do coresspondding receive

     if(myid<SpanBlocks)then
       call MPI_RECV(ImaxCoor_Recv,Inumber,MPI_DOUBLE_PRECISION,myid+SpanBlocks,tag1,MPI_COMM_WORLD,status,ierr)
      
     elseif(myid>=Totalprocs-SpanBlocks)then
       call MPI_SEND(IminCoor_Send,Inumber,MPI_DOUBLE_PRECISION,myid-SpanBlocks,tag1,MPI_COMM_WORLD,ierr)
      
     else
       call MPI_SENDRECV(IminCoor_Send,Inumber,MPI_DOUBLE_PRECISION,myid-SpanBlocks,tag1, &  
                         ImaxCoor_Recv,Inumber,MPI_DOUBLE_PRECISION,myid+SpanBlocks,tag1,MPI_COMM_WORLD,status,ierr  )
     endif



     if(myid<SpanBlocks)then

       do nlap=1,lap
         do j=jb,je
           do k=kb,ke

            x(ie+nlap,j,k)=ImaxCoor_Recv(nlap,j,k,1)
            y(ie+nlap,j,k)=ImaxCoor_Recv(nlap,j,k,2)
            z(ie+nlap,j,k)=ImaxCoor_Recv(nlap,j,k,3)

           enddo
         enddo
       enddo

     elseif(myid>=Totalprocs-SpanBlocks)then
       do nlap=1,lap
         do j=jb,je
           do k=kb,ke

           x(ib-lap+nlap-1,j,k)=IminCoor_Recv(nlap,j,k,1)
           y(ib-lap+nlap-1,j,k)=IminCoor_Recv(nlap,j,k,2)
           z(ib-lap+nlap-1,j,k)=IminCoor_Recv(nlap,j,k,3)
   
           enddo
         enddo
       enddo

     else

       do nlap=1,lap
         do j=jb,je
           do k=kb,ke
           x(ie+nlap,j,k)=ImaxCoor_Recv(nlap,j,k,1)
           y(ie+nlap,j,k)=ImaxCoor_Recv(nlap,j,k,2)
           z(ie+nlap,j,k)=ImaxCoor_Recv(nlap,j,k,3)

           x(ib-lap+nlap-1,j,k)=IminCoor_Recv(nlap,j,k,1)
           y(ib-lap+nlap-1,j,k)=IminCoor_Recv(nlap,j,k,2)
           z(ib-lap+nlap-1,j,k)=IminCoor_Recv(nlap,j,k,3)
           enddo
         enddo
       enddo


     endif


     !!!!!!!!!!!!!!!!!!!!!!!!!!!need to further check 1 start !!!!!!!!!!!!!!!!!!!!!

     if(IminBoundtype==periodic .and. ImaxBoundtype==periodic ) then

      
    
       if(myid>=Totalprocs-SpanBlocks)then
         call MPI_SEND(ImaxCoor_Send,Inumber,MPI_DOUBLE_PRECISION,myid-(StreamBlocks-1)*SpanBlocks,tag2,MPI_COMM_WORLD,ierr)
       elseif(myid<SpanBlocks)then
         call MPI_RECV(IminCoor_Recv,Inumber,MPI_DOUBLE_PRECISION,myid+(StreamBlocks-1)*SpanBlocks,tag2,MPI_COMM_WORLD,status,ierr)
       endif

       if(myid>=Totalprocs-SpanBlocks)then
         call MPI_RECV(ImaxCoor_Recv,Inumber,MPI_DOUBLE_PRECISION,myid-(StreamBlocks-1)*SpanBlocks,tag3,MPI_COMM_WORLD,status,ierr)
       elseif(myid<SpanBlocks)then
         call MPI_SEND(IminCoor_Send,Inumber,MPI_DOUBLE_PRECISION,myid+(StreamBlocks-1)*SpanBlocks,tag3,MPI_COMM_WORLD,ierr)
       endif


      if(myid<SpanBlocks)then

       do nlap=1,lap
         do j=jb,je
           do k=kb,ke

             x(ib-lap+nlap-1,j,k)=IminCoor_Recv(nlap,j,k,1)-DLx_Itransfer
             y(ib-lap+nlap-1,j,k)=IminCoor_Recv(nlap,j,k,2)-DLy_Itransfer
             z(ib-lap+nlap-1,j,k)=IminCoor_Recv(nlap,j,k,3)-DLz_Itransfer

           enddo
         enddo
       enddo

      elseif(myid>=Totalprocs-SpanBlocks)then
       do nlap=1,lap
         do j=jb,je
           do k=kb,ke

             x(ie+nlap,j,k)=ImaxCoor_Recv(nlap,j,k,1)+DLx_Itransfer
             y(ie+nlap,j,k)=ImaxCoor_Recv(nlap,j,k,2)+DLy_Itransfer
             z(ie+nlap,j,k)=ImaxCoor_Recv(nlap,j,k,3)+DLz_Itransfer
   
           enddo
         enddo
       enddo

      endif

     elseif((IminBoundtype==periodic .and. ImaxBoundtype/=periodic).or.(IminBoundtype/=periodic .and. ImaxBoundtype==periodic))then
       write(10,*) "###############################################################################"
       write(10,*) " IminBoundtype and ImaxBoundtype should ste to be periodic at the same time !!!"
       write(10,*) "###############################################################################"
     endif  !! end of if(IminBoundtype==periodic .and. ImaxBoundtype==periodic ) then

      
     !!!!!!!!!!!!!!!!!!!!!!!!!!!need to further check 1 end !!!!!!!!!!!!!!!!!!!!!
     

     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !!!  set coordinates in virtual zoon for J direction.
     !!!  There is no need to send and recieve coordinates in J direction, 
     !!!  since we do not divide blocks in this direction
    if(JminBoundtype==periodic .and. JmaxBoundtype==periodic ) then


     if(myid<SpanBlocks)then

      

       do nlap=1,lap
         do i=ib,ie
           do k=kb,ke

           x(i,jb-lap+nlap-1,k)=x(i,je-lap+nlap-1,k)-DLx_Jtransfer
           y(i,jb-lap+nlap-1,k)=y(i,je-lap+nlap-1,k)-DLy_Jtransfer
           z(i,jb-lap+nlap-1,k)=z(i,je-lap+nlap-1,k)-DLz_Jtransfer

           enddo
        enddo
      enddo

     elseif(myid>=Totalprocs-SpanBlocks)then

       do nlap=1,lap
         do i=ib,ie
           do k=kb,ke

             x(i,je+nlap,k)=x(i,jb+nlap,k)+DLx_Jtransfer
             y(i,je+nlap,k)=y(i,jb+nlap,k)+DLy_Jtransfer
             z(i,je+nlap,k)=z(i,jb+nlap,k)+DLz_Jtransfer         

           enddo
        enddo
      enddo

     endif
    elseif((JminBoundtype==periodic .and. JmaxBoundtype/=periodic).or.(JminBoundtype/=periodic .and. JmaxBoundtype==periodic)) then
       write(10,*) "###############################################################################"
       write(10,*) " JminBoundtype and JmaxBoundtype should set to be periodic at the same time !!!"
       write(10,*) "###############################################################################"
    endif  !! end of if(JminBoundtype==periodic .and. JmaxBoundtype==periodic ) then



     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !!!  sent and receive coordinates for K direction
     
     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !!!    sent and receive coordinates for I direction

     do nlap=1,lap
       do i=ib,ie
         do j=jb,je
        

          KminCoor_Send(i,j,nlap,1)=x(i,j,kb+nlap)
          KminCoor_Send(i,j,nlap,2)=y(i,j,kb+nlap)
          KminCoor_Send(i,j,nlap,3)=z(i,j,kb+nlap)
     
          KmaxCoor_Send(i,j,nlap,1)=x(i,j,ke-lap+nlap-1)
          KmaxCoor_Send(i,j,nlap,2)=y(i,j,ke-lap+nlap-1)
          KmaxCoor_Send(i,j,nlap,3)=z(i,j,ke-lap+nlap-1)

         enddo
       enddo
     enddo


     Knumber=3*lap*Imax*Jmax 
   
     !write(10,*)"Knumber=", Knumber

     !!!!send Kmaxcoordinates for each blocks and do corresspondding receive
      
     if(mod(myid,SpanBlocks)==0)then

       call MPI_SEND(KmaxCoor_Send,Knumber,MPI_DOUBLE_PRECISION,myid+1,tag4,MPI_COMM_WORLD,ierr)
  
     elseif(mod(myid,SpanBlocks)==SpanBlocks-1)then
       call MPI_RECV(KminCoor_Recv,Knumber,MPI_DOUBLE_PRECISION,myid-1,tag4,MPI_COMM_WORLD,status,ierr)
  
     else
       call MPI_SENDRECV(KmaxCoor_Send,Knumber,MPI_DOUBLE_PRECISION,myid+1,tag4, &
                         KminCoor_Recv,Knumber,MPI_DOUBLE_PRECISION,myid-1,tag4,MPI_COMM_WORLD,status,ierr)
     endif



     !!!!send Kmincoordinates for each blocks and do coresspondding receive

     if(mod(myid,SpanBlocks)==0)then
       call MPI_RECV(KmaxCoor_Recv,Knumber,MPI_DOUBLE_PRECISION,myid+1,tag4,MPI_COMM_WORLD,status,ierr)
      
     elseif(mod(myid,SpanBlocks)==SpanBlocks-1)then
       call MPI_SEND(KminCoor_Send,Knumber,MPI_DOUBLE_PRECISION,myid-1,tag4,MPI_COMM_WORLD,ierr)
      
     else
       call MPI_SENDRECV(KminCoor_Send,Knumber,MPI_DOUBLE_PRECISION,myid-1,tag4, &  
                         KmaxCoor_Recv,Knumber,MPI_DOUBLE_PRECISION,myid+1,tag4,MPI_COMM_WORLD,status,ierr  )
     endif



   
     if(mod(myid,SpanBlocks)==0)then
       do nlap=1,lap
         do i=ib,ie
           do j=jb,je
             x(i,j,ke+nlap)=KmaxCoor_Recv(i,j,nlap,1)
             y(i,j,ke+nlap)=KmaxCoor_Recv(i,j,nlap,2)
             z(i,j,ke+nlap)=KmaxCoor_Recv(i,j,nlap,3)
           enddo
         enddo
       enddo
     elseif(mod(myid,SpanBlocks)==SpanBlocks-1)then
       do nlap=1,lap
         do i=ib,ie
           do j=jb,je
             x(i,j,kb-lap+nlap-1)=KminCoor_Recv(i,j,nlap,1)
             y(i,j,kb-lap+nlap-1)=KminCoor_Recv(i,j,nlap,2)
             z(i,j,kb-lap+nlap-1)=KminCoor_Recv(i,j,nlap,3)
           enddo
         enddo
       enddo

     else

       do nlap=1,lap
         do i=ib,ie
           do j=jb,je
             x(i,j,ke+nlap)=KmaxCoor_Recv(i,j,nlap,1)
             y(i,j,ke+nlap)=KmaxCoor_Recv(i,j,nlap,2)
             z(i,j,ke+nlap)=KmaxCoor_Recv(i,j,nlap,3)

             x(i,j,kb-lap+nlap-1)=KminCoor_Recv(i,j,nlap,1)
             y(i,j,kb-lap+nlap-1)=KminCoor_Recv(i,j,nlap,2)
             z(i,j,kb-lap+nlap-1)=KminCoor_Recv(i,j,nlap,3)

           enddo
         enddo
       enddo

     endif  !!! end of  if(mod(myid,SpanBlocks)==0)then


    if(KminBoundtype==periodic .and. KmaxBoundtype==periodic ) then


       if(mod(myid,SpanBlocks)==SpanBlocks-1)then
         call MPI_SEND(KmaxCoor_Send,Knumber,MPI_DOUBLE_PRECISION,myid-(SpanBlocks-1),tag5,MPI_COMM_WORLD,ierr)
       elseif(mod(myid,SpanBlocks)==0)then
         call MPI_RECV(KminCoor_Recv,Knumber,MPI_DOUBLE_PRECISION,myid+(SpanBlocks-1),tag5,MPI_COMM_WORLD,status,ierr)
       endif

       if(mod(myid,SpanBlocks)==SpanBlocks-1)then
         call MPI_RECV(KmaxCoor_Recv,Knumber,MPI_DOUBLE_PRECISION,myid-(SpanBlocks-1),tag6,MPI_COMM_WORLD,status,ierr)
       elseif(mod(myid,SpanBlocks)==0)then
         call MPI_SEND(KminCoor_Send,Knumber,MPI_DOUBLE_PRECISION,myid+(SpanBlocks-1),tag6,MPI_COMM_WORLD,ierr)
       endif


      if(mod(myid,SpanBlocks)==0)then
        do nlap=1,lap
          do i=ib,ie
            do j=jb,je
              x(i,j,kb-lap+nlap-1)=KminCoor_Recv(i,j,nlap,1)-DLx_Ktransfer
              y(i,j,kb-lap+nlap-1)=KminCoor_Recv(i,j,nlap,2)-DLy_Ktransfer
              z(i,j,kb-lap+nlap-1)=KminCoor_Recv(i,j,nlap,3)-DLz_Ktransfer
            enddo
          enddo
        enddo

      elseif(mod(myid,SpanBlocks)==SpanBlocks-1)then

        do nlap=1,lap
          do i=ib,ie
            do j=jb,je
              x(i,j,ke+nlap)=KmaxCoor_Recv(i,j,nlap,1)+DLx_Ktransfer
              y(i,j,ke+nlap)=KmaxCoor_Recv(i,j,nlap,2)+DLy_Ktransfer
              z(i,j,ke+nlap)=KmaxCoor_Recv(i,j,nlap,3)+DLz_Ktransfer
            enddo
          enddo
        enddo

      endif

    elseif((KminBoundtype==periodic .and. KmaxBoundtype/=periodic).or.(KminBoundtype/=periodic .and. KmaxBoundtype==periodic)) then
      write(10,*) "###############################################################################"
      write(10,*) " KminBoundtype and KmaxBoundtype should set to be periodic at the same time !!!"
      write(10,*) "###############################################################################"
    endif   !!! end of if(KminBoundtype==periodic .and. KmaxBoundtype==periodic ) then


      deallocate(IminCoor_Send)
      deallocate(ImaxCoor_Send)

      deallocate(IminCoor_Recv)
      deallocate(ImaxCoor_Recv)


      deallocate(KminCoor_Send)
      deallocate(KmaxCoor_Send)

      deallocate(KminCoor_Recv)
      deallocate(KmaxCoor_Recv)


    end subroutine MPISendRec_PointsCoordinates
    

   




