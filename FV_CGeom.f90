    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!
    !!!! Compute Geometry  variables for Finite Volume Method .... !!!
    !!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    subroutine FV_CGeom
      use main
      implicit none

      real*8:: allx,ally,allz,alrx,alry,alrz
      real*8:: va1,va2,va3,vb1,vb2,vb3
      real*8:: abcdx,abcdy,abcdz,Rabcdx,Rabcdy,Rabcdz
      real*8:: volR,volL
      integer:: i,j,k
     

      !real*8::small1=1.0e-20

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !!!!   compute cell center coordinate for each cell  !!!!!






      do i=1, iq-1
        do j=jb, je-1
          do k=kb ,ke-1

            xc(i,j,k)=0.125*(x(i,j,k)+x(i,j+1,k)+x(i,j,k+1)+x(i,j+1,k+1)+x(i+1,j,k)+x(i+1,j+1,k)+x(i+1,j,k+1)+x(i+1,j+1,k+1))  !!checked
            yc(i,j,k)=0.125*(y(i,j,k)+y(i,j+1,k)+y(i,j,k+1)+y(i,j+1,k+1)+y(i+1,j,k)+y(i+1,j+1,k)+y(i+1,j,k+1)+y(i+1,j+1,k+1))  !!checked
            zc(i,j,k)=0.125*(z(i,j,k)+z(i,j+1,k)+z(i,j,k+1)+z(i,j+1,k+1)+z(i+1,j,k)+z(i+1,j+1,k)+z(i+1,j,k+1)+z(i+1,j+1,k+1))  !!checked

          enddo
        enddo
       enddo

       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       !!!  cell center in the virtual zone at Jmin side
       do i=ib,ie-1
         do j=1,jb-1
           do k=kb, ke-1

            xc(i,j,k)=0.125*(x(i,j,k)+x(i,j+1,k)+x(i,j,k+1)+x(i,j+1,k+1)+x(i+1,j,k)+x(i+1,j+1,k)+x(i+1,j,k+1)+x(i+1,j+1,k+1))  !!checked
            !xc(i,j,k)=0.125*(x(i,j,k)+x(i+1,j,k)+x(i,j,k+1)+x(i+1,j,k+1)+x(i,j+1,k)+x(i+1,j+1,k)+x(i,j+1,k+1)+x(i+1,j+1,k+1))

            yc(i,j,k)=0.125*(y(i,j,k)+y(i,j+1,k)+y(i,j,k+1)+y(i,j+1,k+1)+y(i+1,j,k)+y(i+1,j+1,k)+y(i+1,j,k+1)+y(i+1,j+1,k+1))  !!checked
            zc(i,j,k)=0.125*(z(i,j,k)+z(i,j+1,k)+z(i,j,k+1)+z(i,j+1,k+1)+z(i+1,j,k)+z(i+1,j+1,k)+z(i+1,j,k+1)+z(i+1,j+1,k+1))  !!checked
  
           enddo
         enddo
       enddo

       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       !!!  cell center in the virtual zone at Jmax side
       do i=ib,ie-1
         do j=je,jq-1
           do k=kb,ke-1
   
            xc(i,j,k)=0.125*(x(i,j,k)+x(i,j+1,k)+x(i,j,k+1)+x(i,j+1,k+1)+x(i+1,j,k)+x(i+1,j+1,k)+x(i+1,j,k+1)+x(i+1,j+1,k+1))  !!checked
            yc(i,j,k)=0.125*(y(i,j,k)+y(i,j+1,k)+y(i,j,k+1)+y(i,j+1,k+1)+y(i+1,j,k)+y(i+1,j+1,k)+y(i+1,j,k+1)+y(i+1,j+1,k+1))  !!checked
            zc(i,j,k)=0.125*(z(i,j,k)+z(i,j+1,k)+z(i,j,k+1)+z(i,j+1,k+1)+z(i+1,j,k)+z(i+1,j+1,k)+z(i+1,j,k+1)+z(i+1,j+1,k+1))  !!checked

           enddo
         enddo
       enddo 

       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       !!!  cell center in the virtual zone at Kmin side
       do i=ib,ie-1
         do j=jb,je-1
           do k=1, kb-1

            xc(i,j,k)=0.125*(x(i,j,k)+x(i,j+1,k)+x(i,j,k+1)+x(i,j+1,k+1)+x(i+1,j,k)+x(i+1,j+1,k)+x(i+1,j,k+1)+x(i+1,j+1,k+1))  !!checked
            yc(i,j,k)=0.125*(y(i,j,k)+y(i,j+1,k)+y(i,j,k+1)+y(i,j+1,k+1)+y(i+1,j,k)+y(i+1,j+1,k)+y(i+1,j,k+1)+y(i+1,j+1,k+1))  !!checked
            zc(i,j,k)=0.125*(z(i,j,k)+z(i,j+1,k)+z(i,j,k+1)+z(i,j+1,k+1)+z(i+1,j,k)+z(i+1,j+1,k)+z(i+1,j,k+1)+z(i+1,j+1,k+1))  !!checked
  
           enddo
         enddo
       enddo

       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       !!!  cell center in the virtual zone at Kmax side
       do i=ib,ie-1
         do j=jb,je-1
           do k=ke,kq-1
   
            xc(i,j,k)=0.125*(x(i,j,k)+x(i,j+1,k)+x(i,j,k+1)+x(i,j+1,k+1)+x(i+1,j,k)+x(i+1,j+1,k)+x(i+1,j,k+1)+x(i+1,j+1,k+1))  !!checked
            yc(i,j,k)=0.125*(y(i,j,k)+y(i,j+1,k)+y(i,j,k+1)+y(i,j+1,k+1)+y(i+1,j,k)+y(i+1,j+1,k)+y(i+1,j,k+1)+y(i+1,j+1,k+1))  !!checked
            zc(i,j,k)=0.125*(z(i,j,k)+z(i,j+1,k)+z(i,j,k+1)+z(i,j+1,k+1)+z(i+1,j,k)+z(i+1,j+1,k)+z(i+1,j,k+1)+z(i+1,j+1,k+1))  !!checked

           enddo
         enddo
       enddo 

   
       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       !!!  need to be checked

       !!!! in I direction we just use [ib:ie,jb:je-1,kb:ke-1]
       do i=ib,ie
         do j=kb,ke-1
           do k=kb,ke-1

             allx=0.25*(x(i,j,k)+x(i,j+1,k)+x(i,j,k+1)+x(i,j+1,k+1))-xc(i-1,j,k)
             ally=0.25*(y(i,j,k)+y(i,j+1,k)+y(i,j,k+1)+y(i,j+1,k+1))-yc(i-1,j,k)
             allz=0.25*(z(i,j,k)+z(i,j+1,k)+z(i,j,k+1)+z(i,j+1,k+1))-zc(i-1,j,k)
             allim(i,j,k)=sqrt(allx**2+ally**2+allz**2)

             alrx=0.25*(x(i,j,k)+x(i,j+1,k)+x(i,j,k+1)+x(i,j+1,k+1))-xc(i,j,k)
             alry=0.25*(y(i,j,k)+y(i,j+1,k)+y(i,j,k+1)+y(i,j+1,k+1))-yc(i,j,k)
             alrz=0.25*(z(i,j,k)+z(i,j+1,k)+z(i,j,k+1)+z(i,j+1,k+1))-zc(i,j,k)
             alrim(i,j,k)=sqrt(alrx**2+alry**2+alrz**2)

             !!! debug

#ifdef debug

             if(i==ib) then
               if(j==jb .and. k==kb)then
                 write(10,*) "#######################################################################"
                 write(10,*) "******************Ib boundary:"
                 write(10,*) "#######################################################################"
               endif
               !write(10,*) "x(i,j,k)= ",x(i,j,k),"x(i,j+1,k)= ",x(i,j+1,k),"x(i,j,k+1)= ",x(i,j,k+1),"x(i,j+1,k+1)= ",&
               !           & x(i,j+1,k+1), "xc(i-1,j,k)",xc(i-1,j,k)
               !write(10,*) "y(i,j,k)= ",y(i,j,k),"y(i,j+1,k)= ",y(i,j+1,k),"y(i,j,k+1)= ",y(i,j,k+1),"y(i,j+1,k+1)= ",&
               !           & y(i,j+1,k+1), "yc(i-1,j,k)",yc(i-1,j,k)
               !write(10,*) "z(i,j,k)= ",z(i,j,k),"z(i,j+1,k)= ",z(i,j+1,k),"z(i,j,k+1)= ",z(i,j,k+1),"z(i,j+1,k+1)= ",&
               !           & z(i,j+1,k+1), "zc(i-1,j,k)",zc(i-1,j,k)
               write(10,*) "allim= ",allim(i,j,k),"alrim= ",alrim(i,j,k)
             endif

             if(i==ie)then
               if(j==jb .and. k==kb)then
                 write(10,*) "#######################################################################"
                 write(10,*) "******************Ie boundary:"
                 write(10,*) "#######################################################################"
               endif
               !write(10,*) "x(i,j,k)= ",x(i,j,k),"x(i,j+1,k)= ",x(i,j+1,k),"x(i,j,k+1)= ",x(i,j,k+1),"x(i,j+1,k+1)= ",&
               !           & x(i,j+1,k+1), "xc(i,j,k)",xc(i-1,j,k)
               !write(10,*) "y(i,j,k)= ",y(i,j,k),"y(i,j+1,k)= ",y(i,j+1,k),"y(i,j,k+1)= ",y(i,j,k+1),"y(i,j+1,k+1)= ",&
               !           & y(i,j+1,k+1), "yc(i,j,k)",yc(i-1,j,k)
               !write(10,*) "z(i,j,k)= ",z(i,j,k),"z(i,j+1,k)= ",z(i,j+1,k),"z(i,j,k+1)= ",z(i,j,k+1),"z(i,j+1,k+1)= ",&
               !           & z(i,j+1,k+1), "zc(i,j,k)",zc(i-1,j,k)
               write(10,*) "allim= ",allim(i,j,k),"alrim= ",alrim(i,j,k)
             endif
#endif 
               
           enddo
         enddo
       enddo
       

       !!!! in J direction we just use [ib:ie-1,jb:je,kb:ke-1]
       do j=jb,je
         do i=ib,ie-1
           do k=kb,ke-1

            allx=0.25*(x(i,j,k)+x(i+1,j,k)+x(i,j,k+1)+x(i+1,j,k+1))-xc(i,j-1,k)
            ally=0.25*(y(i,j,k)+y(i+1,j,k)+y(i,j,k+1)+y(i+1,j,k+1))-yc(i,j-1,k)
            allz=0.25*(z(i,j,k)+z(i+1,j,k)+z(i,j,k+1)+z(i+1,j,k+1))-zc(i,j-1,k)
            alljm(i,j,k)=sqrt(allx**2+ally**2+allz**2)  

            alrx=0.25*(x(i,j,k)+x(i+1,j,k)+x(i,j,k+1)+x(i+1,j,k+1))-xc(i,j,k)
            alry=0.25*(y(i,j,k)+y(i+1,j,k)+y(i,j,k+1)+y(i+1,j,k+1))-yc(i,j,k)
            alrz=0.25*(z(i,j,k)+z(i+1,j,k)+z(i,j,k+1)+z(i+1,j,k+1))-zc(i,j,k)
            alrjm(i,j,k)=sqrt(alrx**2+alry**2+alrz**2) 

#ifdef debug

             if(j==jb) then
               if(i==ib .and. k==kb)then
                 write(10,*) "#######################################################################"
                 write(10,*) "******************Jb boundary:"
                 write(10,*) "#######################################################################"
               endif
               !write(10,*) "x(i,j,k)= ",x(i,j,k),"x(i+1,j,k)= ",x(i+1,j,k),"x(i,j,k+1)= ",x(i,j,k+1),"x(i+1,j,k+1)= ",&
               !           & x(i+1,j,k+1), "xc(i,j-1,k)",xc(i,j-1,k)
               !write(10,*) "y(i,j,k)= ",y(i,j,k),"y(i+1,j,k)= ",y(i+1,j,k),"y(i,j,k+1)= ",y(i,j,k+1),"y(i+1,j,k+1)= ",&
               !           & y(i+1,j,k+1), "yc(i,j-1,k)",yc(i,j-1,k)
               !write(10,*) "z(i,j,k)= ",z(i,j,k),"z(i+1,j,k)= ",z(i+1,j,k),"z(i,j,k+1)= ",z(i,j,k+1),"z(i+1,j,k+1)= ",&
               !           & z(i+1,j,k+1), "zc(i,j-1,k)",zc(i,j-1,k)
               write(10,*) "alljm= ",alljm(i,j,k),"alrjm= ",alrjm(i,j,k)
             endif

             if(j==je)then
               if(i==ib .and. k==kb)then
                 write(10,*) "#######################################################################"
                 write(10,*) "******************Je boundary:"
                 write(10,*) "#######################################################################"
               endif
               !write(10,*) "x(i,j,k)= ",x(i,j,k),"x(i+1,j,k)= ",x(i+1,j,k),"x(i,j,k+1)= ",x(i,j,k+1),"x(i+1,j,k+1)= ",&
               !           & x(i+1,j,k+1), "xc(i,j,k)",xc(i,j,k)
               !write(10,*) "y(i,j,k)= ",y(i,j,k),"y(i+1,j,k)= ",y(i+1,j,k),"y(i,j,k+1)= ",y(i,j,k+1),"y(i+1,j,k+1)= ",&
               !           & y(i+1,j,k+1), "yc(i,j,k)",yc(i,j,k)
               !write(10,*) "z(i,j,k)= ",z(i,j,k),"z(i+1,j,k)= ",z(i+1,j,k),"z(i,j,k+1)= ",z(i,j,k+1),"z(i+1,j,k+1)= ",&
               !           & z(i+1,j,k+1), "zc(i,j,k)",zc(i,j,k)
               write(10,*) "alljm= ",alljm(i,j,k),"alrjm= ",alrjm(i,j,k)
             endif
#endif 

           enddo
         enddo
       enddo 



       !!!! in K direction we just use [ib:ie-1,jb:je-1,kb:ke]
       do k=kb,ke
         do i=ib,ie-1
           do j=jb,je-1

            allx=0.25*(x(i,j,k)+x(i+1,j,k)+x(i,j+1,k)+x(i+1,j+1,k))-xc(i,j,k-1)
            ally=0.25*(y(i,j,k)+y(i+1,j,k)+y(i,j+1,k)+y(i+1,j+1,k))-yc(i,j,k-1)
            allz=0.25*(z(i,j,k)+z(i+1,j,k)+z(i,j+1,k)+z(i+1,j+1,k))-zc(i,j,k-1)
            allkm(i,j,k)=sqrt(allx**2+ally**2+allz**2)
 
            alrx=0.25*(x(i,j,k)+x(i+1,j,k)+x(i,j+1,k)+x(i+1,j+1,k))-xc(i,j,k)
            alry=0.25*(y(i,j,k)+y(i+1,j,k)+y(i,j+1,k)+y(i+1,j+1,k))-yc(i,j,k)
            alrz=0.25*(z(i,j,k)+z(i+1,j,k)+z(i,j+1,k)+z(i+1,j+1,k))-zc(i,j,k)
            alrkm(i,j,k)=sqrt(alrx**2+alry**2+alrz**2)

#ifdef debug

             if(k==kb) then
               if(i==ib .and. j==jb)then
                 write(10,*) "#######################################################################"
                 write(10,*) "******************Kb boundary:"
                 write(10,*) "#######################################################################"
               endif
               !write(10,*) "x(i,j,k)= ",x(i,j,k),"x(i+1,j,k)= ",x(i+1,j,k),"x(i,j,k+1)= ",x(i,j+1,k),"x(i+1,j+1,k)= ",&
               !           & x(i+1,j+1,k), "xc(i,j,k)",xc(i,j,k-1)
               !write(10,*) "y(i,j,k)= ",y(i,j,k),"y(i+1,j,k)= ",y(i+1,j,k),"y(i,j,k+1)= ",y(i,j+1,k),"y(i+1,j+1,k)= ",&
               !           & y(i+1,j+1,k), "yc(i,j,k)",yc(i,j,k-1)
               !write(10,*) "z(i,j,k)= ",z(i,j,k),"z(i+1,j,k)= ",z(i+1,j,k),"z(i,j,k+1)= ",z(i,j+1,k),"z(i+1,j+1,k)= ",&
               !          & z(i+1,j+1,k), "zc(i,j,k)",zc(i,j,k-1)
               write(10,*) "allkm= ",allkm(i,j,k),"alrkm= ",alrkm(i,j,k)
             endif

             if(k==ke)then
               if(i==ib .and. j==jb)then
                 write(10,*) "#######################################################################"
                 write(10,*) "******************Ke boundary:"
                 write(10,*) "#######################################################################"
               endif
               !write(10,*) "x(i,j,k)= ",x(i,j,k),"x(i+1,j,k)= ",x(i+1,j,k),"x(i,j,k+1)= ",x(i,j+1,k),"x(i+1,j+1,k)= ",&
               !           & x(i+1,j+1,k), "xc(i,j,k)",xc(i,j,k)
               !write(10,*) "y(i,j,k)= ",y(i,j,k),"y(i+1,j,k)= ",y(i+1,j,k),"y(i,j,k+1)= ",y(i,j+1,k),"y(i+1,j+1,k)= ",&
               !           & y(i+1,j+1,k), "yc(i,j,k)",yc(i,j,k)
               !write(10,*) "z(i,j,k)= ",z(i,j,k),"z(i+1,j,k)= ",z(i+1,j,k),"z(i,j,k+1)= ",z(i,j+1,k),"z(i+1,j+1,k)= ",&
               !           & z(i+1,j+1,k), "zc(i,j,k)",zc(i,j,k)
               write(10,*) "allkm= ",allkm(i,j,k),"alrkm= ",alrkm(i,j,k)
             endif
#endif 

           enddo
         enddo
       enddo



      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !!   for debugging  the following code in I and J dirctions
#ifdef Partialtest1
      ib=4
      jb=4
      kb=4      

      ie=5
      je=5
      ke=5

      x(4,4,4)=1.0
      y(4,4,4)=0.0
      z(4,4,4)=0.0

      x(5,4,4)=2.0
      y(5,4,4)=1.0
      z(5,4,4)=0.0

      x(4,5,4)=0.0
      y(4,5,4)=1.0
      z(4,5,4)=0.0

      x(5,5,4)=1.0
      y(5,5,4)=2.0
      z(5,5,4)=0.0


      x(4,4,5)=1.0
      y(4,4,5)=0.0
      z(4,4,5)=sqrt(2.0)

      x(5,4,5)=2.0
      y(5,4,5)=1.0
      z(5,4,5)=sqrt(2.0)

      x(4,5,5)=0.0
      y(4,5,5)=1.0
      z(4,5,5)=sqrt(2.0)

      x(5,5,5)=1.0
      y(5,5,5)=2.0
      z(5,5,5)=sqrt(2.0)

      xc(4,4,4)=0.125*(x(4,4,4)+x(5,4,4)+x(4,5,4)+x(5,5,4)+x(4,4,5)+x(5,4,5)+x(4,5,5)+x(5,5,5))
      yc(4,4,4)=0.125*(y(4,4,4)+y(5,4,4)+y(4,5,4)+y(5,5,4)+y(4,4,5)+y(5,4,5)+y(4,5,5)+y(5,5,5))
      zc(4,4,4)=0.125*(z(4,4,4)+z(5,4,4)+z(4,5,4)+z(5,5,4)+z(4,4,5)+z(5,4,5)+z(4,5,5)+z(5,5,5))
#endif 
      !!!   for debugging  the following code in I and J dirctions
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !!   for debugging  the following code in J and K dirctions
#ifdef Partialtest2
      ib=4
      jb=4
      kb=4      

      ie=5
      je=5
      ke=5

      x(4,4,4)=0.0
      y(4,4,4)=1.0
      z(4,4,4)=0.0

      x(5,4,4)=sqrt(2.0)
      y(5,4,4)=1.0
      z(5,4,4)=0.0

      x(4,5,4)=0.0
      y(4,5,4)=2.0
      z(4,5,4)=1.0

      x(5,5,4)=sqrt(2.0)
      y(5,5,4)=2.0
      z(5,5,4)=1.0


      x(4,4,5)=0.0
      y(4,4,5)=0.0
      z(4,4,5)=1.0

      x(5,4,5)=sqrt(2.0)
      y(5,4,5)=0.0
      z(5,4,5)=1.0

      x(4,5,5)=0.0
      y(4,5,5)=1.0
      z(4,5,5)=2.0

      x(5,5,5)=sqrt(2.0)
      y(5,5,5)=1.0
      z(5,5,5)=2.0

      xc(4,4,4)=0.125*(x(4,4,4)+x(5,4,4)+x(4,5,4)+x(5,5,4)+x(4,4,5)+x(5,4,5)+x(4,5,5)+x(5,5,5))
      yc(4,4,4)=0.125*(y(4,4,4)+y(5,4,4)+y(4,5,4)+y(5,5,4)+y(4,4,5)+y(5,4,5)+y(4,5,5)+y(5,5,5))
      zc(4,4,4)=0.125*(z(4,4,4)+z(5,4,4)+z(4,5,4)+z(5,5,4)+z(4,4,5)+z(5,4,5)+z(4,5,5)+z(5,5,5))
#endif 
      !!!   for debugging  the following code in J and K dirctions
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !!!!   compute face aera and face aera vector for each cell  !!!!!

      !!!!faces in I direction   
      !!! to ensure that the normal dirction is pionting the I index increasing direction
      do i=ib,ie
        do j=jb,je-1
          do k=kb,ke-1
      !do i=1,iq
        !do j=1,jq-1
          !do k=1,kq-1

            !!!! diagonal line 1
            va1=x(i,j+1,k+1)-x(i,j,k)  !!checked
            va2=y(i,j+1,k+1)-y(i,j,k)  !!checked
            va3=z(i,j+1,k+1)-z(i,j,k)  !!checked
 
            !!!! diagonal line 2
            vb1=x(i,j,k+1)-x(i,j+1,k)  !!checked
            vb2=y(i,j,k+1)-y(i,j+1,k)  !!checked
            vb3=z(i,j,k+1)-z(i,j+1,k)  !!checked
   
            six(i,j,k)=0.5*(va2*vb3-va3*vb2)  !!checked
            siy(i,j,k)=0.5*(va3*vb1-va1*vb3)  !!checked
            siz(i,j,k)=0.5*(va1*vb2-va2*vb1)  !!checked
             si(i,j,k)=sqrt( six(i,j,k)**2 +siy(i,j,k)**2 +siz(i,j,k)**2)  !!checked
#ifdef Partialtest1
            write(10,*) "=====================Test I and J directions======================="
            write(10,*) "six(i,j,k)= ",six(i,j,k),"siy(i,j,k)= ",siy(i,j,k), &
                      & "siz(i,j,k)= ",siz(i,j,k)," si(i,j,k)= ", si(i,j,k)
#endif

#ifdef Partialtest2
            write(10,*) "=====================Test J and K directions======================="
            write(10,*) "six(i,j,k)= ",six(i,j,k),"siy(i,j,k)= ",siy(i,j,k), &
                      & "siz(i,j,k)= ",siz(i,j,k)," si(i,j,k)= ", si(i,j,k)
#endif

          enddo
        enddo
      enddo

      !!!!faces in J direction
      !!! to ensure that the normal dirction is pionting the J index increasing direction
      do i=ib ,ie-1
        do j=jb, je
          do k=kb, ke-1
      !do i=1 ,iq-1
        !do j=1, jq
          !do k=1, kq-1

            !!!! diagonal line 1
            va1=x(i+1,j,k+1)-x(i,j,k)  !!checked
            va2=y(i+1,j,k+1)-y(i,j,k)  !!checked
            va3=z(i+1,j,k+1)-z(i,j,k)  !!checked

            !!!! diagonal line 2
            vb1=x(i+1,j,k)-x(i,j,k+1)  !!checked
            vb2=y(i+1,j,k)-y(i,j,k+1)  !!checked
            vb3=z(i+1,j,k)-z(i,j,k+1)  !!checked
           
            sjx(i,j,k)=0.5*(va2*vb3-va3*vb2)  !!checked
            sjy(i,j,k)=0.5*(va3*vb1-va1*vb3)  !!checked
            sjz(i,j,k)=0.5*(va1*vb2-va2*vb1)  !!checked
             sj(i,j,k)=sqrt(sjx(i,j,k)**2+sjy(i,j,k)**2+sjz(i,j,k)**2)  !!checked

#ifdef Partialtest1
            write(10,*) "=====================Test I and J directions======================="
            write(10,*) "sjx(i,j,k)= ",sjx(i,j,k),"sjy(i,j,k)= ",sjy(i,j,k), &
                      & "sjz(i,j,k)= ",sjz(i,j,k)," sj(i,j,k)= ", sj(i,j,k)
#endif

#ifdef Partialtest2
            write(10,*) "=====================Test J and K directions======================="
            write(10,*) "sjx(i,j,k)= ",sjx(i,j,k),"sjy(i,j,k)= ",sjy(i,j,k), &
                      & "sjz(i,j,k)= ",sjz(i,j,k)," sj(i,j,k)= ", sj(i,j,k)
#endif

          enddo
        enddo
      enddo


      !!!!faces in K direction
      !!! to ensure that the normal dirction is pionting the K index increasing direction
      do i=ib,ie-1
        do j=jb, je-1
          do k=kb, ke
      !do i=1,iq-1
        !do j=1, jq-1
          !do k=1, kq

            !!!! diagonal line 1
            va1=x(i+1,j+1,k)-x(i,j,k)  !!checked
            va2=y(i+1,j+1,k)-y(i,j,k)  !!checked
            va3=z(i+1,j+1,k)-z(i,j,k)  !!checked

            !!!! diagonal line 2
            vb1=x(i,j+1,k)-x(i+1,j,k)  !!checked
            vb2=y(i,j+1,k)-y(i+1,j,k)  !!checked
            vb3=z(i,j+1,k)-z(i+1,j,k)  !!checked

            skx(i,j,k)=0.5*(va2*vb3-va3*vb2)  !!checked
            sky(i,j,k)=0.5*(va3*vb1-va1*vb3)  !!checked
            skz(i,j,k)=0.5*(va1*vb2-va2*vb1)  !!checked
             sk(i,j,k)=sqrt(skx(i,j,k)**2+sky(i,j,k)**2+skz(i,j,k)**2)  !!checked

#ifdef Partialtest1
            write(10,*) "=====================Test I and J directions======================="
            write(10,*) "skx(i,j,k)= ",skx(i,j,k),"sky(i,j,k)= ",sky(i,j,k), &
                      & "skz(i,j,k)= ",skz(i,j,k)," sk(i,j,k)= ", sk(i,j,k)
#endif

#ifdef Partialtest2
            write(10,*) "=====================Test J and K directions======================="
            write(10,*) "skx(i,j,k)= ",skx(i,j,k),"sky(i,j,k)= ",sky(i,j,k), &
                      & "skz(i,j,k)= ",skz(i,j,k)," sk(i,j,k)= ", sk(i,j,k)
#endif

          enddo
        enddo
      enddo


      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !!!!   compute Volume for each cell  !!!!!
      do i=ib, ie-1
        do j=jb, je-1
          do k=kb, ke-1


            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !!!    I direction
  
            abcdx=x(i+1,j,k)+x(i+1,j+1,k)+x(i+1,j,k+1)+x(i+1,j+1,k+1)  !!checked
            abcdy=y(i+1,j,k)+y(i+1,j+1,k)+y(i+1,j,k+1)+y(i+1,j+1,k+1)  !!checked
            abcdz=z(i+1,j,k)+z(i+1,j+1,k)+z(i+1,j,k+1)+z(i+1,j+1,k+1)  !!checked

            Rabcdx=abcdx-4.0*xc(i,j,k)  !!checked
            Rabcdy=abcdy-4.0*yc(i,j,k)  !!checked
            Rabcdz=abcdz-4.0*zc(i,j,k)  !!checked

            volL=six(i+1,j,k)*Rabcdx+siy(i+1,j,k)*Rabcdy+siz(i+1,j,k)*Rabcdz  !!checked
            

     
            abcdx=x(i,j,k)+x(i,j+1,k)+x(i,j,k+1)+x(i,j+1,k+1)  !!checked
            abcdy=y(i,j,k)+y(i,j+1,k)+y(i,j,k+1)+y(i,j+1,k+1)  !!checked
            abcdz=z(i,j,k)+z(i,j+1,k)+z(i,j,k+1)+z(i,j+1,k+1)  !!checked

            Rabcdx=abcdx-4.0*xc(i,j,k)  !!checked
            Rabcdy=abcdy-4.0*yc(i,j,k)  !!checked
            Rabcdz=abcdz-4.0*zc(i,j,k)  !!checked

            volR=six(i,j,k)*Rabcdx+siy(i,j,k)*Rabcdy+siz(i,j,k)*Rabcdz  !!checked
#ifdef Partialtest1
            write(10,*) "=====================Test I and J directions======================="
            write(10,*) "volL= ",volL,"volR= ",volR
                      
#endif

#ifdef Partialtest2
            write(10,*) "=====================Test J and K directions======================="
            write(10,*) "volL= ",volL,"volR= ",volR
#endif
 

            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !!!    J direction

            abcdx=x(i,j+1,k)+x(i+1,j+1,k)+x(i,j+1,k+1)+x(i+1,j+1,k+1)
            abcdy=y(i,j+1,k)+y(i+1,j+1,k)+y(i,j+1,k+1)+y(i+1,j+1,k+1)
            abcdz=z(i,j+1,k)+z(i+1,j+1,k)+z(i,j+1,k+1)+z(i+1,j+1,k+1)
    
            Rabcdx=abcdx-4.0*xc(i,j,k)
            Rabcdy=abcdy-4.0*yc(i,j,k)
            Rabcdz=abcdz-4.0*zc(i,j,k)

            volL=volL+sjx(i,j+1,k)*Rabcdx+sjy(i,j+1,k)*Rabcdy+sjz(i,j+1,k)*Rabcdz


            abcdx=x(i,j,k)+x(i+1,j,k)+x(i,j,k+1)+x(i+1,j,k+1)
            abcdy=y(i,j,k)+y(i+1,j,k)+y(i,j,k+1)+y(i+1,j,k+1)
            abcdz=z(i,j,k)+z(i+1,j,k)+z(i,j,k+1)+z(i+1,j,k+1)

            Rabcdx=abcdx-4.0*xc(i,j,k)
            Rabcdy=abcdy-4.0*yc(i,j,k)
            Rabcdz=abcdz-4.0*zc(i,j,k)

            volR=volR+sjx(i,j,k)*Rabcdx+sjy(i,j,k)*Rabcdy+sjz(i,j,k)*Rabcdz
#ifdef Partialtest1
            write(10,*) "=====================Test I and J directions======================="
            write(10,*) "volL= ",volL,"volR= ",volR
                      
#endif

#ifdef Partialtest2
            write(10,*) "=====================Test J and K directions======================="
            write(10,*) "volL= ",volL,"volR= ",volR
#endif



            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !!!    K direction

            abcdx=x(i,j,k+1)+x(i+1,j,k+1)+x(i,j+1,k+1)+x(i+1,j+1,k+1)
            abcdy=y(i,j,k+1)+y(i+1,j,k+1)+y(i,j+1,k+1)+y(i+1,j+1,k+1)
            abcdz=z(i,j,k+1)+z(i+1,j,k+1)+z(i,j+1,k+1)+z(i+1,j+1,k+1)

            Rabcdx=abcdx-4.0*xc(i,j,k)
            Rabcdy=abcdy-4.0*yc(i,j,k)
            Rabcdz=abcdz-4.0*zc(i,j,k)

            volL=volL+skx(i,j,k+1)*Rabcdx+sky(i,j,k+1)*Rabcdy+skz(i,j,k+1)*Rabcdz


            abcdx=x(i,j,k)+x(i+1,j,k)+x(i,j+1,k)+x(i+1,j+1,k)
            abcdy=y(i,j,k)+y(i+1,j,k)+y(i,j+1,k)+y(i+1,j+1,k)
            abcdz=z(i,j,k)+z(i+1,j,k)+z(i,j+1,k)+z(i+1,j+1,k)
  
            Rabcdx=abcdx-4.0*xc(i,j,k)
            Rabcdy=abcdy-4.0*yc(i,j,k)
            Rabcdz=abcdz-4.0*zc(i,j,k)

            volR=volR+skx(i,j,k)*Rabcdx+sky(i,j,k)*Rabcdy+skz(i,j,k)*Rabcdz
#ifdef Partialtest1
            write(10,*) "=====================Test I and J directions======================="
            write(10,*) "volL= ",volL,"volR= ",volR
                      
#endif

#ifdef Partialtest2
            write(10,*) "=====================Test J and K directions======================="
            write(10,*) "volL= ",volL,"volR= ",volR
#endif

            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !!!    Total!!!
            vol(i,j,k)=(volL-voLR)/12.0

#ifdef Partialtest1
            write(10,*) "=====================Test I and J directions======================="
            write(10,*) "vol(i,j,k)= ",vol(i,j,k)
                      
#endif

#ifdef Partialtest2
            write(10,*) "=====================Test J and K directions======================="
            write(10,*) "vol(i,j,k)= ",vol(i,j,k)

#endif




            if(vol(i,j,k).lt.0)then
              write(10,*)"Aborted Unnormally!!!"
              write(10,*)"Negative volume in zone:",myid,"at cell I, J, K: ",i,j,k
              stop
            endif
 
          enddo
        enddo
      enddo
           

    end subroutine
    


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!
    !!!! Compute Geometry  variables for Finite Difference Method .... !!!
    !!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   
    !subroutine FD_CGeom
      !use main


    !end subroutine



