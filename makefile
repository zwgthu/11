#f77 =/home/zwghit/Workspace/Libs/mpich3/bin/mpif90
#f77=ifort
f77=mpif90
#opt = -fast
FLAGS=-O3 -cpp #-mcmodel=medium
#Dodebug=y  ### if this statement is trigged, then use debugging model
#ifeq ($(Dodebug),y)
	CUSTOM :=$(CUSTOM) -Ddebug
	#CUSTOM :=$(CUSTOM) -DPartialtest1
	#CUSTOM :=$(CUSTOM) -DPartialtest2
#endif

FFLAGS= $(CUSTOM) $(FLAGS)

srs= module.f90 \
     HyperLayer3D.f90 \
     Readmesh.f90 \
     Plot3D_Grid_PreProcess.f90 \
     Readparameters.f90 \
     MPISendRec.f90 \
     FV_Allocate.f90 \
     FV_CGeom.f90 \
     Initialflow.f90 \
     Output.f90 \
     #boundarycondition.f90\
     #flowfield.f90\
     #geometry.f90\
     #initialization.f90\
     #output.f90\
     #post.f90\
     #solver.f90

OBJS=$(srs:.f90=.o)

%.o:%.f90
	$(f77) $(FFLAGS) -c $<

default: $(OBJS)
	$(f77) $(FFLAGS) -o  HyperLayer3D $(OBJS)

clean:
	rm -f HyperLayer3D *.o work.* *.pc *.mod

