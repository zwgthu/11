!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!
!!!!    HyperLayer3D is a 3D CFD code based on High Resolution Finite Volume Method and High Order Finite Difference Method for
!!!!              large-scale parallel simulation of Supersonic/Hypersonic Boundary Layer or Mixing Layer using MPI. 
!!!!             Copyright (C) Dr. Wei-Gang, Zeng  (zwghit@sina.com or cengwg14@tsinghua.org.cn), Tsinghua University  
!!!!                                                         Since 2019.01.01                  
!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    Program HyperLayer3D
     
      use main
      implicit none
      include 'mpif.h' 

      

      call MPI_INIT(ierr)
      call MPI_COMM_RANK(MPI_COMM_WORLD,myid, ierr)
      call MPI_COMM_SIZE(MPI_COMM_WORLD,Totalprocs,ierr)


      if(myid<10)then
        write(LogID,'(i1)')myid
      elseif(myid<100)then
        write(LogID,'(i2)')myid
      elseif(myid<1000)then
        write(LogID,'(i3)')myid
      elseif(myid<10000)then
        write(LogID,'(i4)')myid
      else
        if(myid==0)then
          write(*,*)"The number of total Processes is too large!!!"
        endif
      endif      


      LogDir="LOG"


      inquire(file=trim(LogDir),exist=LogDirhave)

      if(.not.LogDirhave)then

        if(myid==0)then
          call system("mkdir "//trim(LogDir))
        endif
      
      endif
      call MPI_BARRIER(MPI_COMM_WORLD,ierr)

      LogName="LOG_"//trim(LogID)//".dat"


      open(10,form='formatted',file=trim(LogDir)//'/'//trim(LogName))
        write(10,*) "#######################################################################"
        write(10,*) "This is the LOG of Process ID  ",trim(LogID)
        write(10,*) "#######################################################################"

 
        call readparameters
        
       Totalblocks=0
        if(myid==0)then
          call Plot3D_Grid_PreProcess(Totalblocks)
        endif

        call MPI_BARRIER(MPI_COMM_WORLD,ierr)
        call MPI_BCAST(Totalblocks,1,MPI_Integer,0,  MPI_COMM_WORLD,ierr)

       if((Totalblocks .ne. Totalprocs).or.(SpanBlocks*StreamBlocks .ne. Totalprocs).or. &
                                           (SpanBlocks*StreamBlocks .ne. Totalblocks))then
           write(10,*) "#######################################################################"
           write(10,*) "Stop!!! process partition is wrong!!!!"
           write(10,*) "#######################################################################"
           stop
        endif

        call readmesh
    

       if(Numericalframework==FV_Method)then  !!!!integer FV_Method=1

          write(10,*) "#######################################################################"
          write(10,*) "Using Finite Volume Method !!!!"
          write(10,*) "#######################################################################"


          call FV_allocate
          call FV_CGeom    !!!need to transfer position data of coordinates to extend the region for virtual cells
          call FV_initialflow
          Call FV_output_initialflow

        !!do istep=1, maxsteps

          if (issteady==1)then
            if(isexplicit==1)then
             !! call FV_Explicit_advancing
            else
             !! call FV_Implicit_advancing
            endif
          else
            if(isexplicit==1)then
             !! call FV_Explicit_advancing  
            else
             !! call FV_DualTimeImplicit_advancing
            endif
          endif

        !! enddo  !!do istep=1, maxsteps


          !! call FV_deallocate


        elseif(Numericalframework==FD_Method)then  !!! integer FD_Method=2

          write(10,*) "#######################################################################"
          write(10,*) "Using Finite Difference Method !!!!"
          write(10,*) "#######################################################################"
          !! call FD_allocate
          !! call FD_CGeom    !!!need to transfer position data of coordinates to extend the region for virtual cells
          !! call FD_initialflow

          !!do istep=1, maxsteps
          if (issteady==1)then
            if(isexplicit==1)then
              !! call FD_Explicit_advancing
            else
              !! call FD_Implicit_advancing
            endif
          else
            if(isexplicit==1)then
              !! call FD_Explicit_advancing
            else
              !! call FD_DualTimeImplicit_advancing
            endif
          endif

        !! enddo  !!do istep=1, maxsteps

        !! call FD_deallocate

        endif  !!!if(Numericalframework==FV_Method)then  

      close(10)

      
      call MPI_FINALIZE(rc)
      stop

    end program HyperLayer3D
    

   


   


