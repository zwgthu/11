    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!
    !!!! Allocate memory of variables for Finite Volume Method .... !!!
    !!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    subroutine FV_allocate
      use main
      implicit none
      
       write(10,*) "#######################################################################"
       write(10,*) "Allocate memory of Geometry variables for Finite Volume Method .... !!!"
       write(10,*) "#######################################################################"


       allocate( xc(1:iq-1,1:jq-1,1:kq-1))
       allocate( yc(1:iq-1,1:jq-1,1:kq-1))
       allocate( zc(1:iq-1,1:jq-1,1:kq-1))

       !allocate(vol(1:iq-1,1:jq-1,1:kq-1))

       !allocate(  si(1:iq,1:jq-1,1:kq-1))
       !allocate( six(1:iq,1:jq-1,1:kq-1))
       !allocate( siy(1:iq,1:jq-1,1:kq-1))
       !allocate( siz(1:iq,1:jq-1,1:kq-1))

       !allocate(  sj(1:iq-1,1:jq,1:kq-1))
       !allocate( sjx(1:iq-1,1:jq,1:kq-1))
       !allocate( sjy(1:iq-1,1:jq,1:kq-1))
       !allocate( sjz(1:iq-1,1:jq,1:kq-1))

       !allocate(  sk(1:iq-1,1:jq-1,1:kq))
       !allocate( skx(1:iq-1,1:jq-1,1:kq))
       !allocate( sky(1:iq-1,1:jq-1,1:kq))
       !allocate( skz(1:iq-1,1:jq-1,1:kq))


       !allocate( xc(ib:ie-1,jb:je-1,kb:ke-1))
       !allocate( yc(ib:ie-1,jb:je-1,kb:ke-1))
       !allocate( zc(ib:ie-1,jb:je-1,kb:ke-1))
       allocate(vol(ib:ie-1,jb:je-1,kb:ke-1))


       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       !!!  need to be checked
       allocate(allim(1:iq,jb:je-1,kb:ke-1))
       allocate(alrim(1:iq,jb:je-1,kb:ke-1))

       allocate(alljm(ib:ie-1,1:jq,kb:ke-1))
       allocate(alrjm(ib:ie-1,1:jq,kb:ke-1))

       allocate(allkm(ib:ie-1,jb:je-1,1:kq))
       allocate(alrkm(ib:ie-1,jb:je-1,1:kq))
       

  


       allocate(  si(ib:ie,jb:je-1,kb:ke-1))
       allocate( six(ib:ie,jb:je-1,kb:ke-1))
       allocate( siy(ib:ie,jb:je-1,kb:ke-1))
       allocate( siz(ib:ie,jb:je-1,kb:ke-1))

       allocate(  sj(ib:ie-1,jb:je,kb:ke-1))
       allocate( sjx(ib:ie-1,jb:je,kb:ke-1))
       allocate( sjy(ib:ie-1,jb:je,kb:ke-1))
       allocate( sjz(ib:ie-1,jb:je,kb:ke-1))


       allocate(  sk(ib:ie-1,jb:je-1,kb:ke))
       allocate( skx(ib:ie-1,jb:je-1,kb:ke))
       allocate( sky(ib:ie-1,jb:je-1,kb:ke))
       allocate( skz(ib:ie-1,jb:je-1,kb:ke))



       write(10,*) "#######################################################################"
       write(10,*) "Allocate memory of Flow variables for Finite Volume Method ......   !!!"
       write(10,*) "#######################################################################"   
       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       !!! allocate memory for variable at  cell center
       !!! Although the default index of fortran arry begins with 1,
       !!! Here, we still enhance this statement
       allocate(   Qp(1:iq-1,1:jq-1,1:kq-1,0:Nvar))  
       !!!!Here Qp(:,:,:,0)=Tp(:,:,;)

       allocate(   Qc(1:iq-1,1:jq-1,1:kq-1,1:Nvar))
       allocate(Qc_n1(1:iq-1,1:jq-1,1:kq-1,1:Nvar))
       allocate(Qc_n2(1:iq-1,1:jq-1,1:kq-1,1:Nvar))

       allocate(netFlux(ib:ie-1,jb:je-1,kb:ke-1,1:Nvar))

       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       !!! allocate memory for variable at grid edge
       allocate( Flux(ib:ie,jb:je,kb:ke,1:Nvar))
       allocate(   QL(ib:ie,jb:je,kb:ke,1:Nvar)) 
       allocate(   QR(ib:ie,jb:je,kb:ke,1:Nvar))




    end subroutine
    


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!
    !!!! Allocate memory of variables for Finite Difference Method .... !!!
    !!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   
    !subroutine FD_allocate
     ! use main


   ! end subroutine



