    subroutine readparameters
      use main
      implicit none
      open(11,file="HyperLayer3D.in")
        read(11,*) maxsteps, outputsteps
          !! maxsteps:  the maximum advancing steps for current simulation, should be large enough!!!
          !! outputsteps: output the results every outputsteps
        read(11,*) StreamBlocks,SpanBlocks
          !! SectionBlocks: the number of blocks per section
        read(11,*) restartstate, isglobaltime,isexplicit, issteady, Numericalframework
        read(11,*) CFL,timestep, restartsteps, restarttime
        read(11,*) Ma, Re, rho0, u0, v0, w0, Tp0
        read(11,*) T_far
        read(11,*) lap
        read(11,*) IminBoundtype, ImaxBoundtype, JminBoundtype, JmaxBoundtype, KminBoundtype, KmaxBoundtype
        read(11,*) DLx_Itransfer, DLy_Itransfer, DLz_Itransfer
        read(11,*) DLx_Jtransfer, DLy_Jtransfer, DLz_Jtransfer
        read(11,*) DLx_Ktransfer, DLy_Ktransfer, DLz_Ktransfer
      close(11)

    
       gama=1.4
       cp=1.0/(gama-1.0)/Ma/Ma
       cv=cp/gama
       p0=rho0*Tp0/gama/Ma/Ma

     ! if(myid==0)then
       write(10,*) "#######################################################################"
       write(10,*) "The maximun number of loops is  ",maxsteps
       write(10,*) "Output the results per  ", outputsteps, "timesteps"
       write(10,*) "There are ", StreamBlocks, "blocks in streamwise direction and", SpanBlocks,"blocks in spanwise direction"
       if(restartstate==FromVeryBeginning)then
         write(10,*) "This is a new simulation from the very beginning !!!!"
       elseif(restartstate==FromLaststep)then
         write(10,*) "This is a restarting simulation from time ",  restarttime, "; at steps", restartsteps
       endif
       write(10,*) "The CFL number is set to be ", CFL
       write(10,*) "The time step is set to be ", timestep
       write(10,*) "The initial nondimensional velocities are: u0= ", u0, " v0= ", v0, " w0= ", w0
       write(10,*) "The initial nondimensional states are: Tp0= ", Tp0, " rho0= ", rho0, " p0= ", p0
       write(10,*)  "Renolds number= ", Re, " Machnumber= ", Ma
       write(10,*) "The reference temperature is: ", T_far
       write(10,*) "#######################################################################"
         
      !endif

     !!!!!!parameters in the inputfile!!!!!!!!
     !! integer:: maxsteps, outputsteps
     !! integer:: SectionBlocks
     !! integer:: isrestart, isglobaltime,isexplicit
     !! real*8::  CFL,timestep, restartsteps, restarttime
     !! real*8::  u0, v0, w0, Tp0, rho0, Re, Ma   !!!nondimensional values
     !! real*8::  T_far                          !!! dimensional value.
     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   
    end subroutine
    

   




