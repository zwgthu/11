    subroutine FV_output_initialflow
      use main
      implicit none
      include 'mpif.h'
      !include 'mpif.h'

      integer:: i,j,k,ivar
      character (len=20):: s_node_num, s_cells, s_imax, s_jmax, s_kmax   !!!for vtk output
      
      character (len=50)::Initialflowname



      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !!!
      !!!     output grid file for checking : vtk ASCII format 
      !!!
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


      outInitialflowDir="Initialflow_outFile"


      inquire(file=trim(outInitialflowDir),exist=outInitialflowDirhave)

      if(.not.outInitialflowDirhave)then

        if(myid==0)then
          call system("mkdir "//trim(outInitialflowDir))
        endif
      
      endif
      call MPI_BARRIER(MPI_COMM_WORLD,ierr)




      call i4_to_s_left ( (imax-1)*(jmax-1)*(kmax-1), s_node_num )
      call i4_to_s_left ( (imax-2)*(jmax-2)*(kmax-2), s_cells )  ! don't use kmax for 2D
      call i4_to_s_left ( imax-1, s_imax )
      call i4_to_s_left ( jmax-1, s_jmax )
      call i4_to_s_left ( kmax-1, s_kmax )

      Initialflowname="Initialflow_Block_"//trim(LogID)//".vtk"
      !open(output_unit,file=filename,form='formatted')
      open(30,form='formatted',file=trim(outInitialflowDir)//'/'//trim(Initialflowname))

         write ( 30, '(a)' ) '# vtk DataFile Version 2.0 11 22 33 66'
         write ( 30, '(a)' ) 'HyperLayer3D'
         write ( 30, '(a)' ) 'ASCII'
         write ( 30, '(a)' ) 'DATASET STRUCTURED_GRID'
         write ( 30, '(a)' ) 'DIMENSIONS ' // (s_imax) //  (s_jmax) // (s_kmax) 
         write ( 30, '(a)' ) 'POINTS ' // (s_node_num) // 'double'
       do k=kb,ke-1
         do j = jb, je-1
          do i = ib, ie-1
            write ( 30, '(2x,g14.6,2x,g14.6,2x,g14.6)' ) xc(i,j,k),yc(i,j,k),zc(i,j,k)
  
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !!! for testing output
            do ivar=1, Nvar
              Qp(i,j,k,ivar)=i*j*k*ivar
            enddo

          end do
         end do
       enddo

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !! output physical variables

        write ( 30, '(a)' ) 'CELL_DATA ' // (s_cells)
        write ( 30, '(a)' ) 'POINT_DATA ' // (s_node_num)

        write ( 30, '(a)' ) 'SCALARS temperature double '
        write ( 30, '(a)' ) 'LOOKUP_TABLE default '
        write ( 30, *) (((Qp(i,j,k,0),i=ib,ie-1),j=jb,je-1),k=kb,ke-1)

        write ( 30, '(a)' ) 'SCALARS density double '
        write ( 30, '(a)' ) 'LOOKUP_TABLE default '
        write ( 30, *) (((Qp(i,j,k,1),i=ib,ie-1),j=jb,je-1),k=kb,ke-1) 

        write ( 30, '(a)' ) 'SCALARS uvelocity double '
        write ( 30, '(a)' ) 'LOOKUP_TABLE default '
        write ( 30, *) (((Qp(i,j,k,2),i=ib,ie-1),j=jb,je-1),k=kb,ke-1)

        write ( 30, '(a)' ) 'SCALARS vvelocity double '
        write ( 30, '(a)' ) 'LOOKUP_TABLE default '
        write ( 30, *) (((Qp(i,j,k,3),i=ib,ie-1),j=jb,je-1),k=kb,ke-1) 

        write ( 30, '(a)' ) 'SCALARS wvelocity double '
        write ( 30, '(a)' ) 'LOOKUP_TABLE default '
        write ( 30, *) (((Qp(i,j,k,4),i=ib,ie-1),j=jb,je-1),k=kb,ke-1)

        write ( 30, '(a)' ) 'SCALARS pressure double '
        write ( 30, '(a)' ) 'LOOKUP_TABLE default '
        write ( 30, *) (((Qp(i,j,k,5),i=ib,ie-1),j=jb,je-1),k=kb,ke-1)



      close(30)


    end subroutine FV_output_initialflow
    

   




