      subroutine Plot3D_grid_PreProcess(Nblock)
	   implicit none
	   integer:: Nblock
           integer:: i,j,k,iB,N
	   integer:: imaxtmp,jmaxtmp,kmaxtmp
	   integer, pointer, dimension(:):: imax,jmax, kmax,ijkmax
      
	   character*20:: Blockname,Gridname1,Griddirname
	   real,pointer,dimension(:,:,:)::x,y,z
           integer,pointer,dimension(:,:,:)::IBlank
           integer mod_int1,mod_int2

           logical::GridDirhave
             
       
	   open(99, form='formatted',file='xyz.fmt')
           read(99,*) Nblock
	   close(99)



           allocate(imax(Nblock))
           allocate(jmax(Nblock))
           allocate(kmax(Nblock))



          
           Griddirname="Grid"

          !! if(myid==0)then
             inquire(file=trim(Griddirname),exist=GridDirhave)

             if(.not. GridDirhave)then           
               call system("mkdir "//trim(Griddirname))
             endif
           !!endif
           !!call MPI_BARRIER(MPI_COMM_WORLD,ierr)

	   open(199, form='formatted',file='xyz.fmt')
       
           read(199,*) Nblock
	   
	    !write(*,*)Nblock
            read(199,*) (IMAX(iB),JMAX(iB),KMAX(iB),iB=1,Nblock)
            !write(*,*)  (IMAX(iB),JMAX(iB),KMAX(iB),iB=1,Nblock)



            DO N=1,NBLOCK

	    imaxtmp=imax(N)
	    jmaxtmp=jmax(N)
	    kmaxtmp=kmax(N)

            if(N<=10)then
              write(Blockname,'(i1)')N-1
            elseif(N<=100)then
              write(Blockname,'(i2)')N-1
            elseif(N<=1000)then
              write(Blockname,'(i3)')N-1
            elseif(N<=10000)then
              write(Blockname,'(i4)')N-1
            elseif(N<=100000)then
              write(Blockname,'(i5)')N-1
            else
              write(*,*)"The number of total Blocks is too large"
            endif

            Gridname1="Block_"//trim(Blockname)//".grd"
            !write(*,*)Gridname1
            

            allocate(x(imaxtmp,jmaxtmp,kmaxtmp))
            allocate(y(imaxtmp,jmaxtmp,kmaxtmp))
            allocate(z(imaxtmp,jmaxtmp,kmaxtmp))
            allocate(IBlank(imaxtmp,jmaxtmp,kmaxtmp))

            READ(199,*) (((X(I,J,K),I=1,IMAX(N)),J=1,JMAX(N)),K=1,KMAX(N)),&
                        (((Y(I,J,K),I=1,IMAX(N)),J=1,JMAX(N)),K=1,KMAX(N)),&
                        (((Z(I,J,K),I=1,IMAX(N)),J=1,JMAX(N)),K=1,KMAX(N)),&
                        (((IBLANK(I,J,K),I=1,IMAX(N)),J=1,JMAX(N)),K=1,KMAX(N))


            open(299,form='formatted',file=trim(Griddirname)//'/'//trim(Gridname1))
            write(299,*) IMAX(N), JMAX(N), KMAX(N)
            write(299,*) (((X(I,J,K),I=1,IMAX(N)),J=1,JMAX(N)),K=1,KMAX(N))
            write(299,*) (((Y(I,J,K),I=1,IMAX(N)),J=1,JMAX(N)),K=1,KMAX(N))
            write(299,*) (((Z(I,J,K),I=1,IMAX(N)),J=1,JMAX(N)),K=1,KMAX(N))
            close(299)

            deallocate(x)
            deallocate(y)
            deallocate(z)
            deallocate(IBlank)

            ENDDO
    


	     
     
	  close(199)
          
          deallocate(imax)
          deallocate(jmax)
          deallocate(kmax)


  	  
      end subroutine
