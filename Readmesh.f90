    subroutine readmesh
      use main
      implicit none
      include 'mpif.h' 
      !implicit none
      !!write(10,*)"Read mesh for Block ",trim(LogID),myid
      integer:: i,j,k,nlap
      character ( len = 20 ) s_node_num, s_cells, s_imax, s_jmax, s_kmax   !!!for vtk output
      
      Gridname="Block_"//trim(LogID)//".grd"

      open(20,form='formatted',file="Grid"//'/'//trim(Gridname))
      read(20,*)imax,jmax,kmax
      write(10,*) "#######################################################################"
      write(10,*)"imax=", imax, "jmax= ",jmax, "kmax= ", kmax
      write(10,*) "#######################################################################"
      close(20)
   
      iq=imax+2*lap
      jq=jmax+2*lap
      kq=kmax+2*lap
   
      ib=lap+1
      jb=lap+1
      kb=lap+1
 
      ie=iq-lap
      je=jq-lap
      ke=kq-lap

     !!! ie-ib+1=imax, je-jb+1=jmax, ke-kb+1=kmax
     !!! for lap=3, see following example
    
     !!!!!!!!!!!!!!!!!!!!!.......!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     ! 1 ! 2 ! 3 ! 4 ! 5 !.......! ie-1 ! ie  ! ie+1 ! ie+2  ! 
     !!!!!!!!!!!!!!!!!!!!!.......!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !1          ib=4                   ie                 iq=ie+3 
     
      allocate(x(iq,jq,kq))
      allocate(y(iq,jq,kq))
      allocate(z(iq,jq,kq))

      open(20,form='formatted',file="Grid"//'/'//trim(Gridname))
      read(20,*)
      READ(20,*) (((X(I,J,K),I=ib,ie),J=jb,je),K=kb,ke),&
                 (((Y(I,J,K),I=ib,ie),J=jb,je),K=kb,ke),&
                 (((Z(I,J,K),I=ib,ie),J=jb,je),K=kb,ke)             
      close(20)




      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !!!
      !!!     output grid file for checking : tecplot format
      !!!
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      Gridname="Block_"//trim(LogID)//".dat"

      open(30,form='formatted',file="Grid"//'/'//trim(Gridname))

        write(30,*)"variables=x, y, z"
        write(30,*)"zone", " i= ",imax, " j= ", jmax, " k= ", kmax
      
        do k=kb,ke
          do j=jb, je
            do i=ib, ie
             write(30,*)x(i,j,k),y(i,j,k),z(i,j,k)
            enddo
          enddo
       enddo

      close(30)


      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !!!
      !!!     output grid file for checking : vtk ASCII format 
      !!!
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      call i4_to_s_left ( imax*jmax*kmax, s_node_num )
      call i4_to_s_left ( (imax-1)*(jmax-1)*(kmax-1), s_cells )  ! don't use kmax for 2D
      call i4_to_s_left ( imax, s_imax )
      call i4_to_s_left ( jmax, s_jmax )
      call i4_to_s_left ( kmax, s_kmax )

      Gridname="Block_"//trim(LogID)//".vtk"
      !open(output_unit,file=filename,form='formatted')
      open(30,form='formatted',file="Grid"//'/'//trim(Gridname))

         write ( 30, '(a)' ) '# vtk DataFile Version 2.0'
         write ( 30, '(a)' ) 'HyperLayer3D'
         write ( 30, '(a)' ) 'ASCII'
         write ( 30, '(a)' ) 'DATASET STRUCTURED_GRID'
         write ( 30, '(a)' ) 'DIMENSIONS ' // (s_imax) //  (s_jmax) // (s_kmax) 
         write ( 30, '(a)' ) 'POINTS ' // (s_node_num) // 'double'
       do k=kb,ke
         do j = jb, je
          do i = ib, ie
            write ( 30, '(2x,g14.6,2x,g14.6,2x,g14.6)' ) x(i,j,k),y(i,j,k),z(i,j,k)
          end do
         end do
       enddo

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !! output physical variables if needed

        !write ( 30, '(a)' ) 'CELL_DATA ' // (s_cells)
        !write ( 30, '(a)' ) 'POINT_DATA ' // (s_node_num)

        !write ( 30, '(a)' ) 'SCALARS density double '
        !write ( 30, '(a)' ) 'LOOKUP_TABLE default '
        !write ( 30, *) ((prim(i,j,1),i=1,imax),j=1,jmax)  

        !write ( 30, '(a)' ) 'SCALARS uvelocity double '
        !write ( 30, '(a)' ) 'LOOKUP_TABLE default '
        !write ( 30, *) ((prim(i,j,2),i=1,imax),j=1,jmax)  

        !write ( 30, '(a)' ) 'SCALARS vvelocity double '
        !write ( 30, '(a)' ) 'LOOKUP_TABLE default '
        !write ( 30, *) ((prim(i,j,3),i=1,imax),j=1,jmax)  

        !write ( 30, '(a)' ) 'SCALARS temperature double '
        !write ( 30, '(a)' ) 'LOOKUP_TABLE default '
        !write ( 30, *) ((prim(i,j,4),i=1,imax),j=1,jmax)  

        !write ( 30, '(a)' ) 'SCALARS pressure double '
        !write ( 30, '(a)' ) 'LOOKUP_TABLE default '
        !write ( 30, *) ((prim(i,j,5),i=1,imax),j=1,jmax)


      close(30)

     

    

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!     extend coordinates for virtual zoon
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      !!!!extend coordinates for virtual zoon in I direction
       do nlap=1, lap 
         do j=jb, je
           do k=kb, ke
  
             !!! for ib position
             !x(ib-nlap,j,k)=2*x(ib,j,k)-x(ib+nlap,j,k)
             !y(ib-nlap,j,k)=2*y(ib,j,k)-y(ib+nlap,j,k)
             !z(ib-nlap,j,k)=2*z(ib,j,k)-z(ib+nlap,j,k)

            
             
             !!!for ie position
  
             !x(ie+nlap,j,k)=2*x(ie,j,k)-x(ie-nlap,j,k)
             !y(ie+nlap,j,k)=2*y(ie,j,k)-y(ie-nlap,j,k)
             !z(ie+nlap,j,k)=2*z(ie,j,k)-z(ie-nlap,j,k)

              !!! for ib position
              x(ib-nlap,j,k)=2*x(ib-nlap+1,j,k)-x(ib-nlap+2,j,k)
              y(ib-nlap,j,k)=2*y(ib-nlap+1,j,k)-y(ib-nlap+2,j,k)
              z(ib-nlap,j,k)=2*z(ib-nlap+1,j,k)-z(ib-nlap+2,j,k)

              !!!for ie position
              x(ie+nlap,j,k)=2*x(ie+nlap-1,j,k)-x(ie+nlap-2,j,k)
              y(ie+nlap,j,k)=2*y(ie+nlap-1,j,k)-y(ie+nlap-2,j,k)
              z(ie+nlap,j,k)=2*z(ie+nlap-1,j,k)-z(ie+nlap-2,j,k)
     
             
             
    
           enddo
         enddo
       enddo

    !!!!extend coordinates for virtual zoon in J direction
       do i=ib, ie
         do nlap=1, lap
           do k=kb, ke

             !!! for jb position
             x(i,jb-nlap,k)=2*x(i,jb-nlap+1,k)-x(i,jb-nlap+2,k)
             y(i,jb-nlap,k)=2*y(i,jb-nlap+1,k)-y(i,jb-nlap+2,k)
             z(i,jb-nlap,k)=2*z(i,jb-nlap+1,k)-z(i,jb-nlap+2,k)
  
             !!!for je position
             x(i,je+nlap,k)=2*x(i,je+nlap-1,k)-x(i,je+nlap-2,k)
             y(i,je+nlap,k)=2*y(i,je+nlap-1,k)-y(i,je+nlap-2,k)
             z(i,je+nlap,k)=2*z(i,je+nlap-1,k)-z(i,je+nlap-2,k)



           enddo
         enddo
       enddo

     !!!!extend coordinates for virtual zoon in K direction
      do i=ib, ie
         do j=jb, je
           do nlap=1, lap


            !!! for kb position
            x(i,j,kb-nlap)=2*x(i,j,kb-nlap+1)-x(i,j,kb-nlap+2)
            y(i,j,kb-nlap)=2*y(i,j,kb-nlap+1)-y(i,j,kb-nlap+2)
            z(i,j,kb-nlap)=2*z(i,j,kb-nlap+1)-z(i,j,kb-nlap+2)

            !!! for ke position
            x(i,j,ke+nlap)=2*x(i,j,ke+nlap-1)-x(i,j,ke+nlap-2)
            y(i,j,ke+nlap)=2*y(i,j,ke+nlap-1)-y(i,j,ke+nlap-2)
            z(i,j,ke+nlap)=2*z(i,j,ke+nlap-1)-z(i,j,ke+nlap-2)
           enddo
         enddo
      enddo






      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !!!
      !!!     output grid file for checking : vtk ASCII format 
      !!!
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      call i4_to_s_left ( iq*jmax*kmax, s_node_num )
      call i4_to_s_left ( (iq-1)*(jmax-1)*(kmax-1), s_cells )  ! don't use kmax for 2D
      call i4_to_s_left ( iq, s_imax )
      call i4_to_s_left ( jmax, s_jmax )
      call i4_to_s_left ( kmax, s_kmax )

      Gridname="Block_I_"//trim(LogID)//".vtk"
      !open(output_unit,file=filename,form='formatted')
      open(30,form='formatted',file="Grid"//'/'//trim(Gridname))

         write ( 30, '(a)' ) '# vtk DataFile Version 2.0'
         write ( 30, '(a)' ) 'HyperLayer3D'
         write ( 30, '(a)' ) 'ASCII'
         write ( 30, '(a)' ) 'DATASET STRUCTURED_GRID'
         write ( 30, '(a)' ) 'DIMENSIONS ' // (s_imax) //  (s_jmax) // (s_kmax) 
         write ( 30, '(a)' ) 'POINTS ' // (s_node_num) // 'double'
       do k=kb,ke
         do j = jb, je
          do i = 1, iq
            write ( 30, '(2x,g14.6,2x,g14.6,2x,g14.6)' ) x(i,j,k),y(i,j,k),z(i,j,k)
          end do
         end do
       enddo

      close(30)


      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !!!
      !!!     output grid file for checking : vtk ASCII format 
      !!!
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      call i4_to_s_left ( imax*jq*kmax, s_node_num )
      call i4_to_s_left ( (imax-1)*(jq-1)*(kmax-1), s_cells )  ! don't use kmax for 2D
      call i4_to_s_left ( imax, s_imax )
      call i4_to_s_left ( jq, s_jmax )
      call i4_to_s_left ( kmax, s_kmax )

      Gridname="Block_J_"//trim(LogID)//".vtk"
      !open(output_unit,file=filename,form='formatted')
      open(30,form='formatted',file="Grid"//'/'//trim(Gridname))

         write ( 30, '(a)' ) '# vtk DataFile Version 2.0'
         write ( 30, '(a)' ) 'HyperLayer3D'
         write ( 30, '(a)' ) 'ASCII'
         write ( 30, '(a)' ) 'DATASET STRUCTURED_GRID'
         write ( 30, '(a)' ) 'DIMENSIONS ' // (s_imax) //  (s_jmax) // (s_kmax) 
         write ( 30, '(a)' ) 'POINTS ' // (s_node_num) // 'double'
       do k=kb,ke
         do j =1, jq
          do i = ib, ie
            write ( 30, '(2x,g14.6,2x,g14.6,2x,g14.6)' ) x(i,j,k),y(i,j,k),z(i,j,k)
          end do
         end do
       enddo

      close(30)



      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !!!
      !!!     output grid file for checking : vtk ASCII format 
      !!!
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      call i4_to_s_left ( imax*jmax*kq, s_node_num )
      call i4_to_s_left ( (imax-1)*(jmax-1)*(kq-1), s_cells )  ! don't use kmax for 2D
      call i4_to_s_left ( imax, s_imax )
      call i4_to_s_left ( jmax, s_jmax )
      call i4_to_s_left ( kq, s_kmax )

      Gridname="Block_K_"//trim(LogID)//".vtk"
      open(30,form='formatted',file="Grid"//'/'//trim(Gridname))

         write ( 30, '(a)' ) '# vtk DataFile Version 2.0'
         write ( 30, '(a)' ) 'HyperLayer3D'
         write ( 30, '(a)' ) 'ASCII'
         write ( 30, '(a)' ) 'DATASET STRUCTURED_GRID'
         write ( 30, '(a)' ) 'DIMENSIONS ' // (s_imax) //  (s_jmax) // (s_kmax) 
         write ( 30, '(a)' ) 'POINTS ' // (s_node_num) // 'double'
       do k=1,kq
         do j = jb, je
          do i = ib, ie
            write ( 30, '(2x,g14.6,2x,g14.6,2x,g14.6)' ) x(i,j,k),y(i,j,k),z(i,j,k)
          end do
         end do
       enddo


      close(30)


     call MPISendRec_PointsCoordinates
     call MPI_BARRIER(MPI_COMM_WORLD,ierr)


      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !!!
      !!!     output grid file for checking : vtk ASCII format 
      !!!
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      call i4_to_s_left ( iq*jmax*kmax, s_node_num )
      call i4_to_s_left ( (iq-1)*(jmax-1)*(kmax-1), s_cells )  ! don't use kmax for 2D
      call i4_to_s_left ( iq, s_imax )
      call i4_to_s_left ( jmax, s_jmax )
      call i4_to_s_left ( kmax, s_kmax )

      Gridname="Block_IMD_"//trim(LogID)//".vtk"
      !open(output_unit,file=filename,form='formatted')
      open(30,form='formatted',file="Grid"//'/'//trim(Gridname))

         write ( 30, '(a)' ) '# vtk DataFile Version 2.0'
         write ( 30, '(a)' ) 'HyperLayer3D'
         write ( 30, '(a)' ) 'ASCII'
         write ( 30, '(a)' ) 'DATASET STRUCTURED_GRID'
         write ( 30, '(a)' ) 'DIMENSIONS ' // (s_imax) //  (s_jmax) // (s_kmax) 
         write ( 30, '(a)' ) 'POINTS ' // (s_node_num) // 'double'
       do k=kb,ke
         do j = jb, je
          do i = 1, iq
            write ( 30, '(2x,g14.6,2x,g14.6,2x,g14.6)' ) x(i,j,k),y(i,j,k),z(i,j,k)
          end do
         end do
       enddo

      close(30)




      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !!!
      !!!     output grid file for checking : vtk ASCII format 
      !!!
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      call i4_to_s_left ( imax*jmax*kq, s_node_num )
      call i4_to_s_left ( (imax-1)*(jmax-1)*(kq-1), s_cells )  ! don't use kmax for 2D
      call i4_to_s_left ( imax, s_imax )
      call i4_to_s_left ( jmax, s_jmax )
      call i4_to_s_left ( kq, s_kmax )

      Gridname="Block_KMD2_"//trim(LogID)//".vtk"
      open(30,form='formatted',file="Grid"//'/'//trim(Gridname))

         write ( 30, '(a)' ) '# vtk DataFile Version 2.0'
         write ( 30, '(a)' ) 'HyperLayer3D'
         write ( 30, '(a)' ) 'ASCII'
         write ( 30, '(a)' ) 'DATASET STRUCTURED_GRID'
         write ( 30, '(a)' ) 'DIMENSIONS ' // (s_imax) //  (s_jmax) // (s_kmax) 
         write ( 30, '(a)' ) 'POINTS ' // (s_node_num) // 'double'
       do k=1,kq
         do j = jb, je
          do i = ib, ie
            write ( 30, '(2x,g14.6,2x,g14.6,2x,g14.6)' ) x(i,j,k),y(i,j,k),z(i,j,k)
          end do
         end do
       enddo


      close(30)


 
    
     
    end subroutine
    

   

!*****************************************************************************

        subroutine digit_to_ch ( digit, ch )

        implicit none
        character ch
        integer ( kind = 4 ) digit

        if ( 0 <= digit .and. digit <= 9 ) then
          ch = achar ( digit + 48 )
        else
         ch = '*'
         end if

        return
        end

!*****************************************************************************
       subroutine i4_to_s_left ( i4, s )

        implicit none

        character c
        integer ( kind = 4 ) i
        integer ( kind = 4 ) i4
        integer ( kind = 4 ) idig
        integer ( kind = 4 ) ihi
        integer ( kind = 4 ) ilo
        integer ( kind = 4 ) ipos
        integer ( kind = 4 ) ival
        character ( len = * ) s

         s = ' '
         ilo = 1
         ihi = len ( s )
         if ( ihi <= 0 ) then
          return
         end if

         ival = i4

          if ( ival < 0 ) then
           if ( ihi <= 1 ) then
        s(1:1) = '*'
        return
           end if

         ival = -ival
          s(1:1) = '-'
          ilo = 2

         end if

         ipos = ihi

        do

         idig = mod ( ival, 10 )
         ival = ival / 10

           if ( ipos < ilo ) then
       do i = 1, ihi
        s(i:i) = '*'
       end do
       return
        end if

         call digit_to_ch ( idig, c )

         s(ipos:ipos) = c
         ipos = ipos - 1

        if ( ival == 0 ) then
       exit
        end if

         end do

         s(ilo:ilo+ihi-ipos-1) = s(ipos+1:ihi)
         s(ilo+ihi-ipos:ihi) = ' '

         return
        end

!*****************************************************************************   


